package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"strings"

	utils "jeff.com/json_parser/utils"
	converter "jeff.com/json_parser/utils/converter"
)

func generateJsonSerializable(bytes []byte, filenameWithoutExt string) {
	fileSb := strings.Builder{}
	fileSb.WriteString("// GENERATED CODE - DO NOT MODIFY BY HAND\n\n")
	fileSb.WriteString(fmt.Sprintf("part of '%s.dart';\n", filenameWithoutExt))

	className := converter.ToUpperCamelCase(filenameWithoutExt)
	var rootData interface{}
	json.Unmarshal(bytes, &rootData)

	if reflect.TypeOf(rootData).Kind() == reflect.Slice {
		listData := rootData.([]interface{})
		className = fmt.Sprintf("_%sElement", className)
		rootData = listData[0]
	}

	fileSb.WriteString(parseExtensionRecursive(bytes, className, rootData))
	if err := ioutil.WriteFile(filepath.Join(DIST, filenameWithoutExt)+".g.dart", []byte(fileSb.String()), os.ModePerm); err != nil {
		panic(err)
	}
}

func parseExtensionRecursive(sortedBytes []byte, className string, data interface{}) string {
	classSb := strings.Builder{}
	classMap := make(map[string]interface{})

	// FromJson
	classSb.WriteString(fmt.Sprintf("\n%s _$%sFromJson(Map<String, dynamic> json) => %s()", className, converter.ToUpperCamelCase(className), className))

	mapData := data.(map[string]interface{})
	mapKeys := utils.SortMapWithSrc(mapData, sortedBytes)

	for i, key := range mapKeys {
		element := mapData[key]
		elementType := getExtensionAttrsType(key, element, classMap)

		if i == 0 && len(mapKeys) == 1 {
		} else {
			classSb.WriteString("\n")
			classSb.WriteString(INDENT)
		}

		classSb.WriteString(fmt.Sprintf("..%s = json['%s'] == null ? null :", getNewKey(key), key))

		if isBaseType(elementType) {
			classSb.WriteString(fmt.Sprintf(" json['%s'] as %s", key, elementType))
		} else {
			if strings.HasPrefix(elementType, "List<") {
				var listRecursive func(int, string)
				listRecursive = func(count int, input string) {
					classSb.WriteString(fmt.Sprintf(" %s.from(", input))
					if count == 0 {
						classSb.WriteString(fmt.Sprintf("json['%s'].map((x) =>", key))
					} else {
						classSb.WriteString("x.map((x) =>")
					}

					innerClassName := input[strings.Index(input, "<")+1 : strings.LastIndex(input, ">")]
					if strings.HasPrefix(innerClassName, "List<") {
						listRecursive(count+1, innerClassName)
					} else {
						if isBaseType(innerClassName) {
							classSb.WriteString(" x))")
						} else {
							classSb.WriteString(fmt.Sprintf(" %s.fromJson(x)))", innerClassName))
						}
						for i := 0; i < count; i++ {
							classSb.WriteString("))")
						}
					}
				}

				listRecursive(0, elementType)
			} else {
				classSb.WriteString(fmt.Sprintf(" %s.fromJson(json['%s'])", elementType, key))
			}
		}
	}

	classSb.WriteString(";\n\n")

	// ToJson
	classSb.WriteString(fmt.Sprintf("Map<String, dynamic> _$%sToJson(%s instance) => <String, dynamic>{\n", converter.ToUpperCamelCase(className), className))

	for _, key := range mapKeys {
		element := mapData[key]
		elementType := getExtensionAttrsType(key, element, classMap)

		classSb.WriteString("      ")

		newKey := getNewKey(key)
		classSb.WriteString(fmt.Sprintf("if (instance.%s != null) ", newKey))

		classSb.WriteString(fmt.Sprintf("'%s':", converter.ToSnakeCase(key)))

		if isBaseType(elementType) {
			classSb.WriteString(fmt.Sprintf(" instance.%s,\n", newKey))
		} else {
			if strings.HasPrefix(elementType, "List<") {
				classSb.WriteString(fmt.Sprintf(" List<dynamic>.from(instance.%s!.map((x) =>", newKey))
				// classSb.WriteString(fmt.Sprintf(" instance.%s == null ? null : List<dynamic>.from(instance.%s!.map((x) =>", newKey, newKey))

				var listRecursive func(int, string)
				listRecursive = func(count int, input string) {
					innerClassName := input[strings.Index(input, "<")+1 : strings.LastIndex(input, ">")]
					if strings.HasPrefix(innerClassName, "List<") {
						listRecursive(count+1, innerClassName)
					} else {
						for i := 0; i < count; i++ {
							classSb.WriteString(" List<dynamic>.from(x.map((x) =>")
						}

						if isBaseType(innerClassName) {
							classSb.WriteString(" x))")
						} else {
							classSb.WriteString(" x.toJson()))")
						}

						for i := 0; i < count; i++ {
							classSb.WriteString("))")
						}

						classSb.WriteString(",\n")
					}
				}

				listRecursive(0, elementType)
			} else {
				classSb.WriteString(fmt.Sprintf(" instance.%s!.toJson(),\n", newKey))
				// classSb.WriteString(fmt.Sprintf(" instance.%s == null ? null : instance.%s!.toJson(),\n", newKey, newKey))
			}
		}
	}

	classSb.WriteString("    ")
	classSb.WriteString("};\n")

	classKeys := utils.SortMapWithSrc(classMap, sortedBytes)
	for _, key := range classKeys {
		innerClassName := fmt.Sprintf("_%s", converter.ToUpperCamelCase(key))
		classSb.WriteString(parseExtensionRecursive(sortedBytes, innerClassName, classMap[key]))
	}

	return classSb.String()
}

func isBaseType(valueType string) bool {
	switch valueType {
	case "bool":
		return true
	case "num":
		return true
	case "String":
		return true
	default:
		return false
	}
}

func getExtensionAttrsType(key string, value interface{}, classMap map[string]interface{}) string {
	// fmt.Println("key:", key, ", v:", value, ", current:", current)
	switch newValue := value.(type) {
	case bool:
		return "bool"
	case float64:
		return "num"
	case []interface{}:
		innerClassName := getExtensionAttrsType(key, newValue[0], classMap)
		return fmt.Sprintf("List<%s>", innerClassName)
	case map[string]interface{}:
		classMap[key] = value
		innerClassName := fmt.Sprintf("_%s", converter.ToUpperCamelCase(key))
		return innerClassName
	case string:
		if strings.HasPrefix(newValue, "$[]") {
			filename := newValue[3:]
			return fmt.Sprintf("List<%s>", converter.ToUpperCamelCase(filename))
		} else if strings.HasPrefix(newValue, "$") {
			filename := newValue[1:]
			return converter.ToUpperCamelCase(filename)
		} else {
			return "String"
		}
	// case func(int) int:
	default:
		return "String"
	}
}
