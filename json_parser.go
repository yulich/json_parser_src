package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"strings"

	mapset "github.com/deckarep/golang-set"
	utils "jeff.com/json_parser/utils"
	converter "jeff.com/json_parser/utils/converter"
)

const (
	SRC    = `../json`
	DIST   = `../lib/models`
	INDENT = `  `
)

func main() {
	_, err := os.Stat(SRC)
	if errors.Is(err, os.ErrNotExist) {
		fmt.Println("Source directory not exist")
		return
	}

	srcFiles, err := ioutil.ReadDir(SRC)
	if err != nil {
		panic("Get source directory fail")
	}
	if len(srcFiles) == 0 {
		fmt.Println("Source directory is empty")
		return
	}

	var jsonFilenames []string
	for _, f := range srcFiles {
		if filepath.Ext(f.Name()) == ".json" {
			jsonFilenames = append(jsonFilenames, f.Name())
		}
	}

	if len(jsonFilenames) == 0 {
		fmt.Println("Source directory has no json file")
		return
	}

	if _, err = os.Stat(DIST); errors.Is(err, os.ErrNotExist) {
		if err = os.MkdirAll(DIST, os.ModePerm); err != nil {
			panic("Make output directory fail")
		}
	}

	for _, filename := range jsonFilenames {
		bytes, err := ioutil.ReadFile(filepath.Join(SRC, filename))
		if err != nil {
			fmt.Println(err)
			continue
		}
		// fmt.Println(string(byteValue))

		filenameWithoutExt := strings.TrimSuffix(filename, filepath.Ext(filename))

		tagSet := mapset.NewSet()

		parseResult := parseBytes(bytes, filenameWithoutExt, tagSet)

		fileSb := strings.Builder{}

		for _, importString := range tagSet.ToSlice() {
			fileSb.WriteString(importString.(string))
			fileSb.WriteString("\n")
		}
		if tagSet.Cardinality() > 0 {
			fileSb.WriteString("\n")
		}

		fileSb.WriteString(fmt.Sprintf("part '%s.g.dart';\n", filenameWithoutExt))
		fileSb.WriteString(parseResult)

		if err := ioutil.WriteFile(filepath.Join(DIST, filenameWithoutExt)+".dart", []byte(fileSb.String()), os.ModePerm); err != nil {
			panic(err)
		}

		generateJsonSerializable(bytes, filenameWithoutExt)
	}
}

func parseBytes(bytes []byte, filenameWithoutExt string, tagSet mapset.Set) string {
	className := converter.ToUpperCamelCase(filenameWithoutExt)
	var root interface{}
	json.Unmarshal(bytes, &root)

	if reflect.TypeOf(root).Kind() == reflect.Slice {
		arraySb := strings.Builder{}
		arraySb.WriteString(fmt.Sprintf("\nclass %s {\n", className))
		arraySb.WriteString(INDENT)
		arraySb.WriteString(fmt.Sprintf("%s();\n\n", className))

		innerClassName := fmt.Sprintf("_%sElement", className)

		arraySb.WriteString(INDENT)
		arraySb.WriteString(fmt.Sprintf("List<%s>? list;\n\n", innerClassName))

		arraySb.WriteString(INDENT)
		arraySb.WriteString(fmt.Sprintf("factory %s.fromJson(List<dynamic> jsonArray) => %s()..list = jsonArray.map((e) => %s.fromJson(e)).toList();\n", className, className, innerClassName))

		arraySb.WriteString("}\n")

		listData := root.([]interface{})
		arraySb.WriteString(parseClassRecursive(bytes, innerClassName, listData[0], tagSet))
		return arraySb.String()
	} else {
		return parseClassRecursive(bytes, className, root, tagSet)
	}
}

func parseClassRecursive(sortedBytes []byte, className string, data interface{}, tagSet mapset.Set) string {
	classSb := strings.Builder{}
	classMap := make(map[string]interface{})

	classSb.WriteString(fmt.Sprintf("\nclass %s {\n", className))
	classSb.WriteString(INDENT)
	classSb.WriteString(fmt.Sprintf("%s();\n\n", className))

	mapData := data.(map[string]interface{})
	mapKeys := utils.SortMapWithSrc(mapData, sortedBytes)

	for _, key := range mapKeys {
		element := mapData[key]
		elementType := getAttrsType(key, element, className, tagSet, classMap)
		// fmt.Println("K:", key, ", V:", element, ", C:", className)

		classSb.WriteString(INDENT)
		classSb.WriteString(elementType)
		classSb.WriteString("? ")
		classSb.WriteString(getNewKey(key))
		classSb.WriteString(";\n\n")
	}

	funcClassName := converter.ToUpperCamelCase(className)
	classSb.WriteString(INDENT)
	classSb.WriteString(fmt.Sprintf("factory %s.fromJson(Map<String, dynamic> json) => _$%sFromJson(json);\n\n", className, funcClassName))

	classSb.WriteString(INDENT)
	classSb.WriteString(fmt.Sprintf("Map<String, dynamic> toJson() => _$%sToJson(this);\n}\n", funcClassName))

	classKeys := utils.SortMapWithSrc(classMap, sortedBytes)
	for _, key := range classKeys {
		innerClassName := fmt.Sprintf("_%s", converter.ToUpperCamelCase(key))
		classSb.WriteString(parseClassRecursive(sortedBytes, innerClassName, classMap[key], tagSet))
	}

	return classSb.String()
}

func getAttrsType(key string, value interface{}, current string, set mapset.Set, classMap map[string]interface{}) string {
	// fmt.Println("key:", key, ", v:", value, ", current:", current)
	switch newValue := value.(type) {
	case bool:
		return "bool"
	case float64:
		return "num"
	case []interface{}:
		innerClassName := getAttrsType(key, newValue[0], current, set, classMap)
		return fmt.Sprintf("List<%s>", innerClassName)
	case map[string]interface{}:
		classMap[key] = value
		innerClassName := fmt.Sprintf("_%s", converter.ToUpperCamelCase(key))
		return innerClassName
	case string:
		updateImport := func(filename string) {
			if filename != converter.ToSnakeCase(current) {
				set.Add(fmt.Sprintf("import '%s.dart';", filename))
			}
		}

		if strings.HasPrefix(newValue, "$[]") {
			filename := newValue[3:]
			updateImport(filename)
			return fmt.Sprintf("List<%s>", converter.ToUpperCamelCase(filename))
		} else if strings.HasPrefix(newValue, "$") {
			filename := newValue[1:]
			updateImport(filename)
			return converter.ToUpperCamelCase(filename)
		} else {
			return "String"
		}
	// case func(int) int:
	default:
		return "String"
	}
}

func getNewKey(key string) string {
	switch key {
	case "default":
		return "defaultValue"
	default:
		return converter.ToLowerCamelCase(key)
	}
}
