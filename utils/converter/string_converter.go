package utils

import (
	"strings"
	"unicode"
)

func ToUpperCamelCase(s string) string {
	s = strings.Replace(s, "_", " ", -1)
	s = strings.Title(s)
	return strings.Replace(s, " ", "", -1)
}

func ToLowerCamelCase(s string) string {
	s = ToUpperCamelCase(s)
	return string(unicode.ToLower(rune(s[0]))) + s[1:]
}

func ToSnakeCase(s string) string {
	var output []rune
	for i, r := range s {
		if i > 0 && unicode.IsUpper(r) {
			output = append(output, '_')
		}
		output = append(output, unicode.ToLower(r))
	}
	return string(output)
}
