package utils

import (
	"bytes"
	"encoding/json"
	"sort"
)

func SortMapWithSrc(data map[string]interface{}, src []byte) []string {
	var result []string
	// src := []byte(fmt.Sprintf("%v", data))
	index := make(map[string]int)
	for key := range data {
		result = append(result, key)
		esc, _ := json.Marshal(key)
		index[key] = bytes.Index(src, esc)
	}
	sort.Slice(result, func(i, j int) bool { return index[result[i]] < index[result[j]] })
	return result
}
